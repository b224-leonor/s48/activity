// console.log("Hello World!");

// Mock Database
let posts = []

// Posts ID
let count = 1

// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  // Prevents the page from reloading. Also prevents the default behavior of our event.
  e.preventDefault()

  posts.push({
    // "id" property will be used for the unique ID of each posts.
    id: count,
    // "title" and "body" values will come from the "form-add-post" input elements.
    title: document.querySelector('#txt-title').value,
    body: document.querySelector('#txt-body').value,
  })

  // count will increment everytime a new movie is posted.
  count++

  console.log(posts)
  // alert("Movie Successfully Added!");

  showPosts()
})

// RETRIEVE POST
const showPosts = () => {
  // Create a variable that will contain all the posts
  let postEntries = ''

  // We will use forEach() to display each movie inside our mock database.
  posts.forEach((post) => {
    postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
  })

  // To check what is stored in the postEntries variables.
  //console.log(postEntries);

  // To assign the value of postEntries to the element with "div-post-entries" id.
  document.querySelector('#div-post-entries').innerHTML = postEntries
}

// Delete Function

/*document.getElementById(`post-${post.id}`).addEventListener("submit", (e) => {
	e.preventDefault();
	for (let i = 0; i < posts.length; i++) {
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title.splice(i, 1)
			posts[i].body.splice(i, 1)

			showPosts(posts);
			alert("Successfully deleted!");

			break;
		}
	}
});*/

const deletePost = (id) => {
  let id = document.querySelector(`#post-${post.id}`)
  let title = document.querySelector(`#post-title-${id}`)
  let body = document.querySelector(`#post-body-${id}`)

  document.querySelector(`#post-title-${id}`).value = title
  document.querySelector(`#post-body-${id}`).value = body
  console.log()
}

// EDIT POST DATA / Edit Button

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML
  let body = document.querySelector(`#post-body-${id}`).innerHTML

  document.querySelector('#txt-edit-id').value = id
  document.querySelector('#txt-edit-title').value = title
  document.querySelector('#txt-edit-body').value = body
}

// Update post (Update Button)
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
  e.preventDefault()

  for (let i = 0; i < posts.length; i++) {
    if (
      posts[i].id.toString() === document.querySelector('#txt-edit-id').value
    ) {
      posts[i].title = document.querySelector('#txt-edit-title').value
      posts[i].body = document.querySelector('#txt-edit-body').value

      showPosts(posts)
      alert('Successfully updated!')

      break
    }
  }
})

function deletePost(id) {
  // Use the filter method to create a new array with all posts except the one with the specified ID.
  posts = posts.filter((post) => post.id !== id)

  console.log(posts)

  // Use the querySelector method to select the post element with the specified ID and then remove it from the document using the remove method.
  const postElement = document.querySelector(`#post-${id}`)
  if (postElement) {
    postElement.remove()
  }
}
